#include "Q.hpp"
#include <iostream>

using namespace std;
using namespace Q;

template <unsigned int QubitCount>
void print_state(const QuantumRegister<QubitCount> &qregister, bool as_column = true) {
    const char *sep = "";
    for (const auto &state : qregister.states) {
        cout << sep << state;
        sep = as_column ? ",\n" : ", ";
    }
    cout << "\n";
}

template <unsigned int QubitCount>
void print_operator(const QuantumOperator<QubitCount> &qoperator) {
    for (const auto &row : qoperator.states) {
        const char *col_sep = "";
        for (const auto &state : row) {
            cout << col_sep << state;
            col_sep = ", ";
        }
        cout << "\n";
    }
}

// const auto op = Q::Logic::RippleAdder<4>();

int main() {
    auto test_state = ((Q::H ^ Q::H) / 2.0 * Q::QuantumRegister<2>(0)) ^ Q::QuantumRegister<1>(0);
    test_state = Q::CCNOT.exchange<0, 2>() * test_state;

    cout << "Initial state:\n";
    print_state(test_state);
    cout << "\n";

    cout << "Measured qubit 0: " << test_state.measure(0) << "\n\n";

    cout << "State after measuring qubit 0:\n";
    print_state(test_state);
    cout << "\n";

    cout << "Measured qubit 1: " << test_state.measure(1) << "\n\n";

    cout << "State after measuring qubit 1:\n";
    print_state(test_state);
    cout << "\n";

    cout << "Measured qubit 2: " << test_state.measure(2) << "\n\n";

    cout << "State after measuring qubit 2:\n";
    print_state(test_state);
    cout << "\n";

    print_operator(Q::CCNOT);
    cout << "\n";
}
