#ifndef INCLUDE_Q_HPP
#define INCLUDE_Q_HPP

#include "M.hpp"
#include <bit>
#include <bitset>
#include <cmath>
#include <complex>
#include <cstddef>
#include <limits>
#include <numbers>
#include <numeric>
#include <random>
#include <type_traits>
#include <utility>

namespace Q {
    using namespace std::complex_literals;
    using namespace M;

    inline constexpr std::complex<double> complex_round_cartesian(std::complex<double> value, double precision) {
        double multiplier = 1.0 / precision;
        auto new_real = std::round(std::real(value) * multiplier) * precision;
        auto new_imag = std::round(std::imag(value) * multiplier) * precision;
        return 1.0 * new_real + 1.0i * new_imag;
    }

    inline const/*expr*/ std::complex<double> complex_round_polar(std::complex<double> value, double abs_precision, double arg_precision) {
        double abs_multiplier = 1.0 / abs_precision;
        double arg_multiplier = 1.0 / arg_precision;
        auto new_abs = std::round(std::abs(value) * abs_multiplier) * abs_precision;
        auto new_arg = std::round(std::arg(value) * arg_multiplier) * arg_precision;
        return std::polar(new_abs, new_arg);
    }

    template <unsigned int QubitCount,
              typename = std::enable_if_t<(QubitCount > 0)>,
              typename = std::enable_if_t<(QubitCount < std::numeric_limits<std::size_t>::digits)>>
    class QuantumRegister {
    public:
        using states_type = ColVector<std::complex<double>, 1 << QubitCount>;

        constexpr QuantumRegister() : states({1}) {}
        constexpr QuantumRegister(const states_type &states) : states(states) {}
        constexpr QuantumRegister(const states_type &&states) : states(states) {}
        constexpr QuantumRegister(std::size_t state_index) : states({}) { states[state_index] = 1; }

        static constexpr bool qubit_value_for_state(std::size_t state, unsigned int qubit) {
            return std::bitset<QubitCount>(state)[qubit];
        }

        void normalise() {
            auto norm_fold = [](const auto a, const auto b){ return a + std::norm(b); };
            auto norm_squared = std::accumulate(std::begin(states), std::end(states), 0.0, norm_fold);
            auto norm = std::sqrt(norm_squared);

            for (auto &state : states) {
                state /= norm;
            }
        }

        void round_cartesian(double precision) {
            for (auto &state : states) {
                state = complex_round_cartesian(state, precision);
            }
        }

        void round_polar(double abs_precision, double arg_precision) {
            for (auto &state : states) {
                state = complex_round_polar(state, abs_precision, arg_precision);
            }
        }

        bool measure(unsigned int qubit) {
            double prob_zero = 0.0;
            double prob_one = 0.0;

            for (std::size_t state = 0; state < states.size(); ++state) {
                auto probability = std::norm(states[state]);
                if (qubit_value_for_state(state, qubit)) {
                    prob_one += probability;
                } else {
                    prob_zero += probability;
                }
            }

            std::random_device rd;
            std::uniform_real_distribution<> dis(0.0, prob_zero + prob_one);
            bool measured_state;
            double random_value = dis(rd);
            measured_state = random_value >= prob_zero;

            for (std::size_t state = 0; state < states.size(); ++state) {
                if (measured_state != qubit_value_for_state(state, qubit)) {
                    states[state] = 0.0;
                }
            }
            normalise();

            return measured_state;
        }

        std::bitset<QubitCount> measure_all() {
            std::bitset<QubitCount> result{0};

            for (unsigned int qubit = 0; qubit < QubitCount; ++qubit) {
                result[qubit] = measure(qubit);
            }

            return result;
        }

        states_type states;
    };

    using Qubit = QuantumRegister<1>;

    template <unsigned int QubitCountA, unsigned int QubitCountB>
    constexpr QuantumRegister<QubitCountA + QubitCountB> operator^(const QuantumRegister<QubitCountA> &a, const QuantumRegister<QubitCountB> &b) {
        QuantumRegister<QubitCountA + QubitCountB> result;
        result.states = a.states ^ b.states;
        return result;
    }

    template <unsigned int QubitCount,
              typename = std::enable_if_t<(QubitCount > 0)>,
              typename = std::enable_if_t<(QubitCount < std::numeric_limits<std::size_t>::digits)>>
    class QuantumOperator {
    public:
        using states_type = M::Matrix<std::complex<double>, std::size_t(1) << QubitCount, std::size_t(1) << QubitCount>;

        constexpr QuantumOperator() : states({}) {
            for (std::size_t i = 0; i < states.size(); ++i) {
                states[i][i] = 1;
            }
        }
        constexpr QuantumOperator(const states_type &states) : states(states) {}
        constexpr QuantumOperator(const states_type &&states) : states(states) {}

        constexpr void round_cartesian(double precision) {
            for (auto &row : states) {
                for (auto &state : row) {
                    state = complex_round_cartesian(state, precision);
                }
            }
        }

        constexpr void round_polar(double abs_precision, double arg_precision) {
            for (auto &row : states) {
                for (auto &state : row) {
                    state = complex_round_polar(state, abs_precision, arg_precision);
                }
            }
        }

        constexpr QuantumOperator dagger() const {
            QuantumOperator<QubitCount> result;

            for (std::size_t i = 0; i < states.size(); ++i) {
                for (std::size_t j = 0; j < states[i].size(); ++j) {
                    result.states[i][j] = states[j][i];
                }
            }

            return result;
        }

        constexpr QuantumOperator<QubitCount + 1> controlled(bool inverse = false) const {
            QuantumOperator<QubitCount + 1> result;

            if (!inverse) {
                for (std::size_t row = 0; row < states.size(); ++row) {
                    std::copy(states[row].begin(), states[row].end(), result.states[row + states.size()].begin() + states[row].size());
                }
            } else {
                for (std::size_t row = 0; row < states.size(); ++row) {
                    std::copy(states[row].begin(), states[row].end(), result.states[row].begin());
                }
            }

            return result;
        }

        template <unsigned int Before, unsigned int After, typename std::enable_if_t<Before != 0 && After != 0, int> = 0>
        constexpr QuantumOperator<QubitCount + Before + After> expand() const {
            return QuantumOperator<1>() ^ expand<Before - 1, After - 1>() ^ QuantumOperator<1>();
        }
        template <unsigned int Before, unsigned int After, typename std::enable_if_t<Before == 0 && After != 0, int> = 0>
        constexpr QuantumOperator<QubitCount + Before + After> expand() const {
            return expand<Before, After - 1>() ^ QuantumOperator<1>();
        }
        template <unsigned int Before, unsigned int After, typename std::enable_if_t<Before != 0 && After == 0, int> = 0>
        constexpr QuantumOperator<QubitCount + Before + After> expand() const {
            return QuantumOperator<1>() ^ expand<Before - 1, After>();
        }
        template <unsigned int Before, unsigned int After, typename std::enable_if_t<Before == 0 && After == 0, int> = 0>
        constexpr QuantumOperator<QubitCount + Before + After> expand() const {
            return *this;
        }

        template <unsigned int At, typename = std::enable_if_t<(At < QubitCount - 1)>>
        constexpr QuantumOperator swap() const {
            constexpr QuantumOperator<2> swap_operator{{{{
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 },
            }}}};

            return swap_operator.expand<At, QubitCount - At - 2>() * (*this) * swap_operator.expand<At, QubitCount - At - 2>();
        }

        template <unsigned int At,
                  typename std::enable_if_t<(At < QubitCount - 1), int> = 0>
        constexpr QuantumOperator slide() const {
            return this->slide<At + 1>().template swap<At>();
        }
        template <unsigned int At,
                  typename std::enable_if_t<At == QubitCount - 1, int> = 0>
        constexpr QuantumOperator slide() const {
            return *this;
        }

        template <unsigned int At, unsigned int Inserted,
                  typename std::enable_if_t<(At <= QubitCount), int> = 0,
                  typename std::enable_if_t<Inserted != 0, int> = 0>
        constexpr QuantumOperator<QubitCount + Inserted> split() const {
            return this->expand<0, 1>().template slide<At>().template split<At, Inserted - 1>();
        }
        template <unsigned int At, unsigned int Inserted,
                  typename std::enable_if_t<(At <= QubitCount), int> = 0,
                  typename std::enable_if_t<Inserted == 0, int> = 0>
        constexpr QuantumOperator<QubitCount + Inserted> split() const {
            return *this;
        }

        template <unsigned int Times,
                  typename std::enable_if_t<(Times > 1), int> = 0,
                  typename std::enable_if_t<(Times <= std::numeric_limits<decltype(Times)>::max() / QubitCount), int> = 0>
        constexpr QuantumOperator<Times * QubitCount> repeat() const {
            return *this ^ this->repeat<Times - 1>();
        }
        template <unsigned int Times,
                  typename std::enable_if_t<(Times == 1), int> = 0>
        constexpr QuantumOperator<Times * QubitCount> repeat() const {
            return *this;
        }

        template <unsigned int IndexA,
                  unsigned int IndexB,
                  typename = std::enable_if_t<(IndexA < IndexB)>,
                  typename = std::enable_if_t<(IndexB < QubitCount)>>
        constexpr QuantumOperator exchange() const {
            constexpr QuantumOperator<2> swap_operator{{{{
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 },
            }}}};
            constexpr auto swapper = swap_operator.expand<IndexA, QubitCount - IndexB - 1>().template split<IndexA + 1, IndexB - IndexA - 1>();

            return swapper * (*this) * swapper;
        }

        template <class none = void>
        constexpr QuantumOperator flip() const {
            return *this;
        }
        template <unsigned int FirstIndex,
                  unsigned int... RestOfIndices,
                  typename std::enable_if_t<(FirstIndex < QubitCount), int> = 0>
        constexpr QuantumOperator flip() const {
            constexpr QuantumOperator<1> not_operator{{{{
                { 0.0, 1.0 },
                { 1.0, 0.0 },
            }}}};
            constexpr auto qubit_flip_operator = not_operator.expand<FirstIndex, QubitCount - FirstIndex - 1>();
            auto is_flipped = qubit_flip_operator * (*this);

            return is_flipped.template flip<RestOfIndices...>();
        }

        template <unsigned int... Indices>
        constexpr QuantumOperator flip_unflip() const {
            constexpr auto flipper{QuantumOperator<QubitCount>{}.template flip<Indices...>()};
            return flipper * (*this) * flipper;
        }

        states_type states;
    };

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount>& operator+=(QuantumOperator<QubitCount> &a, const QuantumOperator<QubitCount> &b) {
        a.states += b.states;
        return a;
    }

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount>& operator-=(QuantumOperator<QubitCount> &a, const QuantumOperator<QubitCount> &b) {
        a.states -= b.states;
        return a;
    }

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount>& operator*=(QuantumOperator<QubitCount> &a, const QuantumOperator<QubitCount> &b) {
        a.states *= b.states;
        return a;
    }

    template <unsigned int QubitCount, typename T>
    constexpr QuantumOperator<QubitCount>& operator*=(QuantumOperator<QubitCount> &a, const T b) {
        a.states *= b;
        return a;
    }

    template <unsigned int QubitCount, typename T>
    constexpr QuantumOperator<QubitCount>& operator/=(QuantumOperator<QubitCount> &a, const T b) {
        a.states /= b;
        return a;
    }

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount> operator+(const QuantumOperator<QubitCount> &a, const QuantumOperator<QubitCount> &b) {
        QuantumOperator<QubitCount> result;
        result.states = a.states + b.states;
        return result;
    }

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount> operator-(const QuantumOperator<QubitCount> &a, const QuantumOperator<QubitCount> &b) {
        QuantumOperator<QubitCount> result;
        result.states = a.states - b.states;
        return result;
    }

    template <unsigned int QubitCount>
    constexpr QuantumRegister<QubitCount> operator*(const QuantumOperator<QubitCount> &a, const QuantumRegister<QubitCount> &b) {
        QuantumRegister<QubitCount> result;
        result.states = a.states * b.states;
        return result;
    }

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount> operator*(const QuantumOperator<QubitCount> &a, const QuantumOperator<QubitCount> &b) {
        QuantumOperator<QubitCount> result;
        result.states = a.states * b.states;
        return result;
    }

    template <unsigned int QubitCount, typename T>
    constexpr QuantumOperator<QubitCount> operator*(const QuantumOperator<QubitCount> &a, const T b) {
        QuantumOperator<QubitCount> result;
        result.states = a.states * b;
        return result;
    }

    template <unsigned int QubitCount, typename T>
    constexpr QuantumOperator<QubitCount> operator*(const T a, const QuantumOperator<QubitCount> &b) {
        return b * a;
    }

    template <unsigned int QubitCount, typename T>
    constexpr QuantumOperator<QubitCount> operator/(const QuantumOperator<QubitCount> &a, const T b) {
        QuantumOperator<QubitCount> result;
        result.states = a.states / b;
        return result;
    }

    template <unsigned int QubitCount>
    constexpr QuantumOperator<QubitCount> operator~(const QuantumOperator<QubitCount> &x) {
        return x.dagger();
    }

    template <unsigned int QubitCountA, unsigned int QubitCountB>
    constexpr QuantumOperator<QubitCountA + QubitCountB> operator^(const QuantumOperator<QubitCountA> &a, const QuantumOperator<QubitCountB> &b) {
        QuantumOperator<QubitCountA + QubitCountB> result;
        result.states = a.states ^ b.states;
        return result;
    }

    // QuantumOperator<1>
    inline constexpr QuantumOperator<1> I{{{{
        { 1.0, 0.0 },
        { 0.0, 1.0 },
    }}}};
    inline constexpr QuantumOperator<1> X{{{{
        { 0.0, 1.0 },
        { 1.0, 0.0 },
    }}}};
    inline constexpr QuantumOperator<1> Y{{{{
        { 0.0, -1.0i },
        { 1.0i, 0.0 },
    }}}};
    inline constexpr QuantumOperator<1> Z{{{{
        { 1.0, 0.0 },
        { 0.0, -1.0 },
    }}}};
    inline const/*expr*/ QuantumOperator<1> R_x(double theta) {
        return std::cos(-theta) * I + (1.0i * std::sin(-theta)) * X;
    }
    inline const/*expr*/ QuantumOperator<1> R_y(double theta) {
        return std::cos(-theta) * I + (1.0i * std::sin(-theta)) * Y;
    }
    inline const/*expr*/ QuantumOperator<1> R_z(double phi) {
        return std::cos(-phi) * I + (1.0i * std::sin(-phi)) * Z;
    }
    inline const/*expr*/ QuantumOperator<1> R(double phi) {
        return R_z(phi);
    }
    inline constexpr QuantumOperator<1> S{{{{
        { 1.0, 0.0 },
        { 0.0, 1.0i },
    }}}};
    inline constexpr QuantumOperator<1> T{{{{
        { 1.0, 0.0 },
        { 0.0, 1.0 / std::numbers::sqrt2 + 1.0i / std::numbers::sqrt2 },
    }}}};
    inline constexpr QuantumOperator<1> H{{{{
        { 1.0 / std::numbers::sqrt2, 1.0 / std::numbers::sqrt2 },
        { 1.0 / std::numbers::sqrt2, -1.0 / std::numbers::sqrt2 },
    }}}};
    inline constexpr QuantumOperator<1> h{{{{
        { 1.0 / std::numbers::sqrt2, 1.0 / std::numbers::sqrt2 },
        { -1.0 / std::numbers::sqrt2, 1.0 / std::numbers::sqrt2 },
    }}}};
    inline constexpr QuantumOperator<1> NOT = X;
    inline constexpr QuantumOperator<1> SQRT_NOT{{{{
        { (1.0 + 1.0i) / 2.0, (1.0 - 1.0i) / 2.0 },
        { (1.0 - 1.0i) / 2.0, (1.0 + 1.0i) / 2.0 },
    }}}};

    // QuantumOperator<2>
    inline constexpr QuantumOperator<2> CX = X.controlled();
    inline constexpr QuantumOperator<2> CY = Y.controlled();
    inline constexpr QuantumOperator<2> CZ = Z.controlled();
    inline constexpr QuantumOperator<2> CNOT = NOT.controlled();
    inline const/*expr*/ QuantumOperator<2> B(double phase) { return R(phase).controlled(); }
    inline constexpr QuantumOperator<2> SWAP{{{{
        { 1.0, 0.0, 0.0, 0.0 },
        { 0.0, 0.0, 1.0, 0.0 },
        { 0.0, 1.0, 0.0, 0.0 },
        { 0.0, 0.0, 0.0, 1.0 },
    }}}};
    inline constexpr QuantumOperator<2> iSWAP{{{{
        { 1.0, 0.0, 0.0, 0.0 },
        { 0.0, 0.0, 1.0i, 0.0 },
        { 0.0, 1.0i, 0.0, 0.0 },
        { 0.0, 0.0, 0.0, 1.0 },
    }}}};
    inline constexpr QuantumOperator<2> SQRT_SWAP{{{{
        { 1.0, 0.0, 0.0, 0.0 },
        { 0.0, (1.0 + 1.0i) / 2.0, (1.0 - 1.0i) / 2.0, 0.0 },
        { 0.0, (1.0 - 1.0i) / 2.0, (1.0 + 1.0i) / 2.0, 0.0 },
        { 0.0, 0.0, 0.0, 1.0 },
    }}}};
    inline const/*expr*/ QuantumOperator<2> CAN(double theta_x, double theta_y, double phi) {
        std::complex<double> sin_neg_theta_x_i = 1.0i * std::sin(-theta_x);
        std::complex<double> cos_neg_theta_x_r = 1.0  * std::cos(-theta_x);
        std::complex<double> sin_neg_theta_y_i = 1.0i * std::sin(-theta_y);
        std::complex<double> cos_neg_theta_y_r = 1.0  * std::cos(-theta_y);
        std::complex<double> sin_neg_phi_i     = 1.0i * std::sin(-phi);
        std::complex<double> cos_neg_phi_r     = 1.0  * std::cos(-phi);

        return cos_neg_theta_x_r * cos_neg_theta_y_r * cos_neg_phi_r * ((I ^ I)) +
               sin_neg_theta_x_i * cos_neg_theta_y_r * cos_neg_phi_r * ((X ^ X)) +
               cos_neg_theta_x_r * sin_neg_theta_y_i * cos_neg_phi_r * ((Y ^ Y)) +
               cos_neg_theta_x_r * cos_neg_theta_y_r * sin_neg_phi_i * ((Z ^ Z)) +
               sin_neg_theta_x_i * sin_neg_theta_y_i * cos_neg_phi_r * ((X ^ X) * (Y ^ Y)) +
               sin_neg_theta_x_i * cos_neg_theta_y_r * sin_neg_phi_i * ((X ^ X) * (Z ^ Z)) +
               cos_neg_theta_x_r * sin_neg_theta_y_i * sin_neg_phi_i * ((Y ^ Y) * (Z ^ Z)) +
               sin_neg_theta_x_i * sin_neg_theta_y_i * sin_neg_phi_i * ((X ^ X) * (Y ^ Y) * (Z ^ Z));
    }
    inline const/*expr*/ QuantumOperator<2> XX(std::complex<double> theta) {
        return std::cos(-theta) * (I ^ I) + (1.0i * std::sin(-theta)) * (X ^ X);
    }
    inline const/*expr*/ QuantumOperator<2> YY(std::complex<double> theta) {
        return std::cos(-theta) * (I ^ I) + (1.0i * std::sin(-theta)) * (Y ^ Y);
    }
    inline const/*expr*/ QuantumOperator<2> ZZ(std::complex<double> phi) {
        return std::cos(-phi) * (I ^ I) + (1.0i * std::sin(-phi)) * (Z ^ Z);
    }

    // QuantumOperator<3>
    inline constexpr QuantumOperator<3> CCNOT = CNOT.controlled();
    inline constexpr QuantumOperator<3> CSWAP = SWAP.controlled();

    // QuantumOperator<N>
    template <unsigned int N, typename = std::enable_if_t<std::has_single_bit(N)>>
    inline constexpr QuantumOperator<std::countr_zero(N)> F(std::complex<double> phase = 1.0i) {
        QuantumOperator<std::countr_zero(N)> result;

        for (size_t row = 0; row < result.states.size(); ++row) {
            for (size_t col = 0; col < result.states[row].size(); ++col) {
                result.states[row][col] = std::pow(phase, row * col) / std::sqrt(N);
            }
        }

        return result;
    }

    // QuantumRegister<1>
    inline constexpr QuantumRegister<1> Zero = QuantumRegister<1>(0);
    inline constexpr QuantumRegister<1> One = QuantumRegister<1>(1);

    // QuantumRegister<2>
    inline constexpr QuantumRegister<2> Bell(unsigned int state = 0) { return CNOT * H.expand<0, 1>() * QuantumRegister<2>(state); }
    inline constexpr QuantumRegister<2> Bell(QuantumRegister<2> init_state) { return CNOT * H.expand<0, 1>() * init_state; }

    namespace Logic {
        // {a, z} -> {a, z ^ (a)}
        inline constexpr QuantumOperator<2> BUFFER = CNOT;
        // {a, z} -> {a, z ^ ~(a)}
        inline constexpr QuantumOperator<2> NOT = BUFFER.flip<1>();
        // {a, b, z} -> {a, b, z ^ (a & b)}
        inline constexpr QuantumOperator<3> AND = CCNOT;
        // {a, b, z} -> {a, b, z ^ ~(a & b)}
        inline constexpr QuantumOperator<3> NAND = AND.flip<2>();
        // {a, b, z} -> {a, b, z ^ (a | b)}
        inline constexpr QuantumOperator<3> OR = NAND.flip_unflip<0, 1>();
        // {a, b, z} -> {a, b, z ^ ~(a | b)}
        inline constexpr QuantumOperator<3> NOR = OR.flip<2>();
        // {a, b, z} -> {a, b, z ^ (a ^ b)}
        inline constexpr QuantumOperator<3> XOR = CNOT.split<1, 1>() * CNOT.expand<1, 0>();
        // {a, b, z} -> {a, b, z ^ ~(a ^ b)}
        inline constexpr QuantumOperator<3> XNOR = XOR.flip<2>();
        // {a, b, z} -> {a, b, z ^ (a -> b)}
        inline constexpr QuantumOperator<3> IMPLY = OR.flip_unflip<0>();
        // {a, b, z} -> {a, b, z ^ ~(a -> b)}
        inline constexpr QuantumOperator<3> NIMPLY = IMPLY.flip<2>();

        // {a, b, z} -> {a, s, z ^ c}
        inline constexpr QuantumOperator<3> HalfAdder =
            CNOT.expand<0, 1>() * CCNOT;

        // {a, b, c_in, z} -> {a, b, s, z ^ c_out}
        inline constexpr QuantumOperator<4> FullAdder =
            CNOT.expand<0, 2>() * CNOT.expand<1, 1>() * CCNOT.expand<1, 0>() * CNOT.expand<0, 2>() * CCNOT.split<2, 1>();

        // Based on work in [arXiv:quant-ph/0410184]
        // {c_i, b_i, a_i} -> {c_i ^ a_i, b_i ^ a_i, c_i+1}
        inline constexpr QuantumOperator<3> MAJ = 
            CCNOT * CNOT.swap<0>().split<1, 1>() * CNOT.swap<0>().expand<1, 0>();
        // {c_i ^ a_i, b_i ^ a_i, c_i+1} -> {c_i, s_i, a_i}
        inline constexpr QuantumOperator<3> UMA = 
            CNOT.expand<0, 1>() * CNOT.swap<0>().split<1, 1>() * CCNOT;
        // {c_0, z} -> {c_0, z ^ c_0}
        template <unsigned int Width,
                  typename std::enable_if_t<(Width == 0), int> = 0>
        inline constexpr QuantumOperator<2 * Width + 2> RippleAdder() { return CNOT; }
        // {c_0, b_0, a_0, b_1, a_1, ..., b_N-1, a_N-1, z} ->
        // {c_0, s_0, a_0, s_1, a_1, ..., s_N-1, a_N-1, z ^ s_N}
        template <unsigned int Width,
                  typename std::enable_if_t<(Width <= (std::numeric_limits<decltype(Width)>::max() - 2) / 2), int> = 0,
                  typename std::enable_if_t<(Width > 0), int> = 0>
        inline constexpr QuantumOperator<2 * Width + 2> RippleAdder() {
            return UMA.expand<0, 2 * Width - 1>() *
                   RippleAdder<Width - 1>().template expand<2, 0>() *
                   MAJ.expand<0, 2 * Width - 1>();
        }
    }

    namespace Error {
        inline constexpr QuantumOperator<3> BitFlipCode =
            CNOT.split<1, 1>() * CNOT.expand<0, 1>();
        inline constexpr QuantumOperator<3> BitFlipUncode =
            CCNOT.exchange<0, 2>() * CNOT.split<1, 1>() * CNOT.expand<0, 1>();

        inline constexpr QuantumOperator<3> SignFlipCode =
            H.repeat<3>() * BitFlipCode;
        inline constexpr QuantumOperator<3> SignFlipUncode =
            BitFlipUncode * H.repeat<3>();

        inline constexpr auto ShorCode_0 = BitFlipCode ^ BitFlipCode;
        inline constexpr auto ShorCode_1 = ShorCode_0 ^ BitFlipCode;
        inline constexpr auto ShorCode_2 = SignFlipCode.split<3, 2>().split<2, 2>().split<1, 2>();
        // inline constexpr QuantumOperator<9> ShorCode = ShorCode_0 * ShorCode_1;
        inline constexpr auto ShorUncode_0 = SignFlipUncode.split<3, 2>().split<2, 2>().split<1, 2>();
        inline constexpr auto ShorUncode_1 = BitFlipUncode ^ BitFlipUncode;
        inline constexpr auto ShorUncode_2 = ShorUncode_1 ^ BitFlipUncode;
        // inline constexpr QuantumOperator<9> ShorUncode = ShorUncode_0 * ShorUncode_1;
    }
}

#endif
